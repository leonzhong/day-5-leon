package com.parkinglot;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ParkingLot {
    private List<ParkingTicket> ticketList;
    private final int capacity;

    public ParkingLot() {
        this.capacity = 10;
        this.ticketList = new ArrayList<>();
    }

    public ParkingLot(int capacity) {
        this.ticketList = new ArrayList<>();
        this.capacity = capacity;
    }

    public ParkingTicket park(Car car) {
        if (isNotAvailablePosition()) {
            ParkingTicket parkingTicket = new ParkingTicket(car);
            ticketList.add(parkingTicket);
            return parkingTicket;
        }
        throw  new NoAvailablePosition();
    }

    private boolean isNotAvailablePosition() {
        return this.capacity > this.ticketList.size();
    }

    public Car fetch(ParkingTicket ticket) {
        ParkingTicket filterTicket = ticketList.stream()
                .filter(ticketed -> ticketed
                        .equals(ticket))
                .findFirst()
                .orElse(null);
        if (Objects.nonNull(filterTicket)) {
            ticketList.remove(filterTicket);
            return filterTicket.getCar();
        }
        throw new UnRecognizedParkingTicketException();
    }

    public List<ParkingTicket> getTicketList() {
        return ticketList;
    }

    public int getCapacity() {
        return capacity;
    }
}
