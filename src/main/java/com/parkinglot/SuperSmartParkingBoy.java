package com.parkinglot;

import java.util.Arrays;
import java.util.Comparator;

public class SuperSmartParkingBoy extends SmartParkingBoy {

    public SuperSmartParkingBoy(ParkingLot... parkingLots) {
        super(parkingLots);
    }

    @Override
    public ParkingTicket parkCar(Car car) {
        ParkingLot fetchedParkingLot = Arrays.stream(parkingLot)
                .max(Comparator.comparingDouble(parkingLot ->
                        (double)(parkingLot.getCapacity() - parkingLot.getTicketList().size()) / parkingLot.getCapacity()))
                .orElseThrow(NoAvailablePosition::new);

        return fetchedParkingLot.park(car);
    }

}