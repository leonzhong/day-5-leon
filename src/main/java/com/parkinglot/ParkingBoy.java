package com.parkinglot;

import java.util.Arrays;
import java.util.Optional;

public class ParkingBoy {
    protected final ParkingLot[] parkingLot;

    public ParkingBoy(ParkingLot... parkingLot) {
        this.parkingLot = parkingLot;
    }

    public ParkingTicket parkCar(Car car) {
        return Arrays.stream(parkingLot)
                .filter(parkingLot ->
                        parkingLot.getCapacity() > parkingLot.getTicketList().size()
                )
                .findFirst()
                .orElseThrow(NoAvailablePosition::new)
                .park(car);
    }

    public Car fetchCar(ParkingTicket ticket) {
        Optional<ParkingLot> filterParkingLot = Arrays.stream(parkingLot)
                .filter(parkingLot -> parkingLot.getTicketList().contains(ticket))
                .findFirst();
        return filterParkingLot
                .orElseThrow(UnRecognizedParkingTicketException::new)
                .fetch(ticket);
    }
}
