package com.parkinglot;

public class UnRecognizedParkingTicketException extends RuntimeException {
    public UnRecognizedParkingTicketException() {
        super("Unrecognized parking ticket.");
    }
}
