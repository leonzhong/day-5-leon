package com.parkinglot.strategy;

import com.parkinglot.NoAvailablePosition;

import java.util.Arrays;
import java.util.Comparator;

public enum ParkStrategy {
    SEQUENTIAL((parkingLot, car) -> Arrays.stream(parkingLot)
            .filter(lot ->
                    lot.getCapacity() > lot.getTicketList().size()
            )
            .findFirst()
            .orElseThrow(NoAvailablePosition::new)
            .park(car)),
    MOSTLY((parkingLot, car) ->
            Arrays.stream(parkingLot)
                    .max(Comparator.comparingInt(lot -> lot.getCapacity() - lot.getTicketList().size()))
                    .orElseThrow(NoAvailablePosition::new).park(car)),
    MOST_SPACE_RATE((parkingLot, car) ->
            Arrays.stream(parkingLot)
                    .max(Comparator.comparingDouble(lot ->
                            (double) (lot.getCapacity() - lot.getTicketList().size()) / lot.getCapacity()))
                    .orElseThrow(NoAvailablePosition::new).park(car));

    private final ParkingStrategy strategy;

    ParkStrategy(ParkingStrategy parkingStrategy) {
        this.strategy = parkingStrategy;
    }

    public ParkingStrategy getStrategy() {
        return strategy;
    }
}
