package com.parkinglot.strategy;

import com.parkinglot.Car;
import com.parkinglot.ParkingLot;
import com.parkinglot.ParkingTicket;

public class ParkingBoyStrategy {
    protected final ParkingLot[] parkingLot;
    private final ParkingStrategy strategy;

    public ParkingBoyStrategy(ParkStrategy strategy, ParkingLot... parkingLot) {
        this.parkingLot = parkingLot;
        this.strategy = strategy.getStrategy();
    }

    // 使用策略
    public ParkingTicket parkCar(Car car) {
        return strategy.selectParkingLot(parkingLot, car);
    }


}
