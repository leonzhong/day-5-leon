package com.parkinglot.strategy;

import com.parkinglot.Car;
import com.parkinglot.ParkingLot;
import com.parkinglot.ParkingTicket;

@FunctionalInterface
public interface ParkingStrategy {

    ParkingTicket selectParkingLot(ParkingLot[] parkingLots,Car car);
}