package com.parkinglot;

import java.util.Arrays;
import java.util.Comparator;

public class SmartParkingBoy extends ParkingBoy{
    public SmartParkingBoy(ParkingLot... parkingLots) {
        super(parkingLots);
    }
    @Override
    public ParkingTicket parkCar(Car car) {
        // Find parking lot with most empty spots
        ParkingLot mostEmptyLot = Arrays.stream(parkingLot)
                .max(Comparator.comparingInt(parkingLot -> parkingLot.getCapacity() - parkingLot.getTicketList().size()))
                .orElseThrow(NoAvailablePosition::new);

        return mostEmptyLot.park(car);
    }
}
