package com.parkinglot;

import com.parkinglot.strategy.ParkStrategy;
import com.parkinglot.strategy.ParkingBoyStrategy;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ParkingBoyStrategyTest {
    @Test
    void should_park_car_to_first_parking_lot_when_park_given_two_parking_lots_and_car_and_parking_boy_and_first_parking_lot_emptiest() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(5);
        ParkingBoyStrategy parkingBoy = new ParkingBoyStrategy(ParkStrategy.SEQUENTIAL, parkingLot1, parkingLot2);
        Car car = new Car();
        //when
        ParkingTicket parkingTicket = parkingBoy.parkCar(car);
        // then
        Assertions.assertTrue(parkingLot1.getTicketList().contains(parkingTicket));
        Assertions.assertFalse(parkingLot2.getTicketList().contains(parkingTicket));
    }

    @Test
    void should_park_car_to_second_parking_lot_when_park_given_two_parking_lots_and_car_and_smart_parking_boy_and_second_parking_lot_emptiest() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(5);
        ParkingBoyStrategy smartParkingBoy = new ParkingBoyStrategy(ParkStrategy.MOSTLY, parkingLot1, parkingLot2);
        Car car = new Car();
        //when
        ParkingTicket parkingTicket = smartParkingBoy.parkCar(car);
        // then
        Assertions.assertTrue(parkingLot2.getTicketList().contains(parkingTicket));
        Assertions.assertFalse(parkingLot1.getTicketList().contains(parkingTicket));
    }

    @Test
    void should_park_car_to_second_parking_lot_when_park_given_two_parking_lots_and_car_and_super_smart_parking_boy_and_second_parking_lot_emptiest() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(5);
        ParkingBoyStrategy superSmartParkingBoy = new ParkingBoyStrategy(ParkStrategy.MOST_SPACE_RATE, parkingLot1, parkingLot2);
        Car car = new Car();
        //when
        superSmartParkingBoy.parkCar(new Car());
        ParkingTicket parkingTicket = superSmartParkingBoy.parkCar(car);
        // then
        Assertions.assertTrue(parkingLot2.getTicketList().contains(parkingTicket));
        Assertions.assertFalse(parkingLot1.getTicketList().contains(parkingTicket));
    }

}
