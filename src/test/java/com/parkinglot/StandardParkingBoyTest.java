package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class StandardParkingBoyTest {
    /**
     * parking1 contains parkingTicket
     * parking2 not contains parkingTicket
     */
    @Test
    void should_park_car_to_first_parking_lot_when_park_given_two_parking_lots_and_car_and_standard_parking_boy_and_first_parking_lot_available() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot1, parkingLot2);
        Car car = new Car();
        //when
        ParkingTicket parkingTicket = parkingBoy.parkCar(car);
        // then
        Assertions.assertTrue(parkingLot1.getTicketList().contains(parkingTicket));
        Assertions.assertFalse(parkingLot2.getTicketList().contains(parkingTicket));
    }

    /**
     * when parking1 not available
     * parking2 contains parkingTicket
     * parking1 not contains parkingTicket
     */
    @Test
    void should_park_car_to_second_parking_lot_when_park_given_two_parking_lots_and_car_and_standard_parking_boy_and_first_parking_lot_not_available_and_second_parking_lot_available() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot1, parkingLot2);
        Car car = new Car();
        parkingBoy.parkCar(new Car());
        //when
        ParkingTicket parkingTicket = parkingBoy.parkCar(car);
        // then
        Assertions.assertFalse(parkingLot1.getTicketList().contains(parkingTicket));
        Assertions.assertTrue(parkingLot2.getTicketList().contains(parkingTicket));
    }

    @Test
    void should_return_right_car_when_fetch_given_two_parking_lots_and_two_car_and_two_parking_ticket_and_standard_parking_boy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot1, parkingLot2);
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingTicket parkingTicket1 = parkingBoy.parkCar(car1);
        ParkingTicket parkingTicket2 = parkingBoy.parkCar(car2);
        //when
        Car fetchedCar1 = parkingBoy.fetchCar(parkingTicket1);
        Car fetchedCar2 = parkingBoy.fetchCar(parkingTicket2);

        // then
        Assertions.assertEquals(fetchedCar1, car1);
        Assertions.assertEquals(fetchedCar2, car2);
    }

    @Test
    void should_throw_error_when_fetch_given_two_parking_lots_and_one_unrecognized_parking_ticket_and_standard_parking_boy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot1, parkingLot2);
        Car car1 = new Car();
        parkingBoy.parkCar(car1);
        ParkingTicket parkingTicket2 = new ParkingTicket(new Car());
        //when
        // then
        UnRecognizedParkingTicketException exception = Assertions.assertThrows(UnRecognizedParkingTicketException.class, () -> parkingBoy.fetchCar(parkingTicket2));
        Assertions.assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }


    @Test
    void should_park_car_to_first_parking_lot_when_park_given_two_parking_lots_and_car_and_standard_parking_boy_and_first_parking_lot_not_available_and_second_parking_lot_available() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot1, parkingLot2);
        //when
        ParkingTicket parkingTicket1 = parkingBoy.parkCar(new Car());
        parkingBoy.parkCar(new Car());

        parkingBoy.fetchCar(parkingTicket1);

        ParkingTicket parkingTicket3 = parkingBoy.parkCar(new Car());

        // then
        Assertions.assertTrue(parkingLot1.getTicketList().contains(parkingTicket3));
    }

    //    Given a standard parking boy, who manage two parking lots, and a used ticket, When
//     fetch the car, Then return nothing with error message "Unrecognized parking ticket.
    @Test
    void should_throw_error_when_fetch_given_two_parking_lots_and_one_used_parking_ticket_and_standard_parking_boy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot1, parkingLot2);
        Car car1 = new Car();
        ParkingTicket parkingTicket = parkingBoy.parkCar(car1);
        parkingBoy.fetchCar(parkingTicket);
        //when
        // then
        UnRecognizedParkingTicketException exception = Assertions.assertThrows(UnRecognizedParkingTicketException.class, () -> parkingBoy.fetchCar(parkingTicket));
        Assertions.assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    void should_throw_error_when_park_given_two_parking_lots_and_car_and_without_any_position_and_standard_parking_boy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot1, parkingLot2);
        //when
        parkingBoy.parkCar(new Car());
        parkingBoy.parkCar(new Car());
        // then
        NoAvailablePosition exception = Assertions.assertThrows(NoAvailablePosition.class, () -> parkingBoy.parkCar(new Car()));
        Assertions.assertEquals("No available position.", exception.getMessage());
    }

}
