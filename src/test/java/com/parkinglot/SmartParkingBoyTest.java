package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SmartParkingBoyTest {
    @Test
    void should_park_car_to_first_parking_lot_when_park_given_two_parking_lots_and_car_and_smart_parking_boy_and_first_parking_lot_emptiest() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(5);
        ParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot1, parkingLot2);
        Car car = new Car();
        //when
        ParkingTicket parkingTicket = smartParkingBoy.parkCar(car);
        // then
        Assertions.assertTrue(parkingLot1.getTicketList().contains(parkingTicket));
        Assertions.assertFalse(parkingLot2.getTicketList().contains(parkingTicket));
    }

    @Test
    void should_park_car_to_second_parking_lot_when_park_given_two_parking_lots_and_car_and_smart_parking_boy_and_second_parking_lot_emptiest() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(5);
        ParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot1, parkingLot2);
        Car car = new Car();
        //when
        ParkingTicket parkingTicket = smartParkingBoy.parkCar(car);
        // then
        Assertions.assertTrue(parkingLot2.getTicketList().contains(parkingTicket));
        Assertions.assertFalse(parkingLot1.getTicketList().contains(parkingTicket));
    }

    @Test
    void should_return_right_car_when_fetch_given_two_parking_lots_and_two_car_and_two_parking_ticket_and_smart_parking_boy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(5);
        ParkingLot parkingLot2 = new ParkingLot(6);
        ParkingBoy parkingBoy = new SmartParkingBoy(parkingLot1, parkingLot2);
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingTicket parkingTicket1 = parkingBoy.parkCar(car1);
        ParkingTicket parkingTicket2 = parkingBoy.parkCar(car2);
        //when
        Car fetchedCar1 = parkingBoy.fetchCar(parkingTicket1);
        Car fetchedCar2 = parkingBoy.fetchCar(parkingTicket2);

        // then
        Assertions.assertEquals(fetchedCar1, car1);
        Assertions.assertEquals(fetchedCar2, car2);
    }
    @Test
    void should_throw_error_when_fetch_given_two_parking_lots_and_one_unrecognized_parking_ticket_and_smart_parking_boy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        ParkingBoy parkingBoy = new SmartParkingBoy(parkingLot1, parkingLot2);
        Car car1 = new Car();
        parkingBoy.parkCar(car1);
        ParkingTicket parkingTicket2 = new ParkingTicket(new Car());
        //when
        // then
        UnRecognizedParkingTicketException exception = Assertions.assertThrows(UnRecognizedParkingTicketException.class, () -> parkingBoy.fetchCar(parkingTicket2));
        Assertions.assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }
    @Test
    void should_throw_error_when_fetch_given_two_parking_lots_and_one_used_parking_ticket_and_smart_parking_boy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        ParkingBoy parkingBoy = new SmartParkingBoy(parkingLot1, parkingLot2);
        Car car1 = new Car();
        ParkingTicket parkingTicket = parkingBoy.parkCar(car1);
        parkingBoy.fetchCar(parkingTicket);
        //when
        // then
        UnRecognizedParkingTicketException exception = Assertions.assertThrows(UnRecognizedParkingTicketException.class, () -> parkingBoy.fetchCar(parkingTicket));
        Assertions.assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    void should_throw_error_when_park_given_two_parking_lots_and_car_and_without_any_position_and_smart_parking_boy() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(1);
        ParkingBoy parkingBoy = new SmartParkingBoy(parkingLot1, parkingLot2);
        //when
        parkingBoy.parkCar(new Car());
        parkingBoy.parkCar(new Car());
        // then
        NoAvailablePosition exception = Assertions.assertThrows(NoAvailablePosition.class, () -> parkingBoy.parkCar(new Car()));
        Assertions.assertEquals("No available position.", exception.getMessage());
    }
}
