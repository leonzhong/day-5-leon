package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ParkingLotTest {
    @Test
    void should_return_ticket_when_park_given_parking_lot_and_car() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        //when
        ParkingTicket ticket = parkingLot.park(car);

        //then
        Assertions.assertNotNull(ticket);
    }

    @Test
    void should_return_car_when_fetch_given_parking_lot_and_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        ParkingTicket ticket = parkingLot.park(car);
        //when
        Car fetchedCar = parkingLot.fetch(ticket);
        //then
        Assertions.assertEquals(car, fetchedCar);
    }

    @Test
    void should_return_right_car_when_fetch_given_parking_lot_and_two_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        Car secCar = new Car();
        ParkingTicket ticket = parkingLot.park(car);
        ParkingTicket secTicket = parkingLot.park(secCar);
        //when
        Car fetchedCar = parkingLot.fetch(ticket);
        Car fetchedSecCar = parkingLot.fetch(secTicket);

        //then
        Assertions.assertEquals(car, fetchedCar);
        Assertions.assertEquals(secCar, fetchedSecCar);
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_parking_lot_and_wrong_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        parkingLot.park(car);
        ParkingTicket wrongTicket = new ParkingTicket(new Car());
        //when
        UnRecognizedParkingTicketException exception = Assertions.assertThrows(UnRecognizedParkingTicketException.class, () -> parkingLot.fetch(wrongTicket));
        Assertions.assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_parking_lot_and_used_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();

        ParkingTicket parkingTicket = parkingLot.park(car);

        //when
        parkingLot.fetch(parkingTicket);
        UnRecognizedParkingTicketException exception = Assertions.assertThrows(UnRecognizedParkingTicketException.class, () -> parkingLot.fetch(parkingTicket));
        Assertions.assertEquals("Unrecognized parking ticket.", exception.getMessage());


    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_parking_lot_and_without_any_position() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        Car car = new Car();
        //when
        for (int i = 0; i < parkingLot.getCapacity(); i++) {
            parkingLot.park(new Car());
        }

        NoAvailablePosition exception = Assertions.assertThrows(NoAvailablePosition.class, () -> parkingLot.park(car));
        Assertions.assertEquals("No available position.", exception.getMessage());

    }
    @Test
    void should_return_ticket_when_park_given_parking_lot_and_car_and_parking_boy() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        //when
        ParkingTicket ticket = parkingBoy.parkCar(car);

        //then
        Assertions.assertNotNull(ticket);
    }

    @Test
    void should_return_car_when_fetch_given_parking_lot_and_ticket_and_parking_boy() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);

        ParkingTicket ticket = parkingBoy.parkCar(car);
        //when
        Car fetchedCar = parkingBoy.fetchCar(ticket);
        //then
        Assertions.assertEquals(car, fetchedCar);
    }

    @Test
    void should_return_right_car_when_fetch_given_parking_lot_and_two_ticket_and_parking_boy() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        Car secCar = new Car();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);

        ParkingTicket ticket = parkingBoy.parkCar(car);
        ParkingTicket secTicket = parkingBoy.parkCar(secCar);
        //when
        Car fetchedCar = parkingBoy.fetchCar(ticket);
        Car fetchedSecCar = parkingBoy.fetchCar(secTicket);

        //then
        Assertions.assertEquals(car, fetchedCar);
        Assertions.assertEquals(secCar, fetchedSecCar);
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_parking_lot_and_wrong_ticket_and_parking_boy() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);

        parkingBoy.parkCar(car);
        ParkingTicket wrongTicket = new ParkingTicket(new Car());
        //when
        UnRecognizedParkingTicketException exception = Assertions.assertThrows(UnRecognizedParkingTicketException.class, () -> parkingBoy.fetchCar(wrongTicket));
        Assertions.assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_parking_lot_and_used_ticket_and_parking_boy() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);


        ParkingTicket parkingTicket = parkingBoy.parkCar(car);

        //when
        parkingBoy.fetchCar(parkingTicket);
        UnRecognizedParkingTicketException exception = Assertions.assertThrows(UnRecognizedParkingTicketException.class, () -> parkingBoy.fetchCar(parkingTicket));
        Assertions.assertEquals("Unrecognized parking ticket.", exception.getMessage());


    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_parking_lot_and_without_any_position_and_parking_boy() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        Car car = new Car();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);

        //when
        for (int i = 0; i < parkingLot.getCapacity(); i++) {
            parkingBoy.parkCar(new Car());
        }

        NoAvailablePosition exception = Assertions.assertThrows(NoAvailablePosition.class, () -> parkingBoy.parkCar(car));
        Assertions.assertEquals("No available position.", exception.getMessage());

    }
}
