### Objective:
- Today we worked on a combination of OOP and TTD, working through the requirements step by step from Story1 to Story6, stacking up and increasing in difficulty.

### Reflective:
- I feel gradually recognised TTD .
### Interpretive:
- The good thing about TTD is that from shallow to deep, even if we do refactoring later, we are still able to carry out verification of the test cases at the beginning to ensure that our development is in the right direction. In our work, we will iterate on top of the old code, how to ensure that our code does not affect the original business? In addition to ensuring that code is non-intrusive, running previous test cases is more reassuring.
### Decision:
- Combine design patterns with TTD and OOP in my learning and keep verifying what I have learnt.